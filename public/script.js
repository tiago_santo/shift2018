var EFFECT_TIME = 250;
var GAME_LOOP_TIME = 1000;


var game_loop;

var my_username = "Player";
var action_list = [];

var last_game_status = "";
var last_item = 0;

var changed = false;


$(function () {

    showMainMenu();
    //showClientGame();
});


/* ******************************************* */


function newGame() {
    hideAll();
    showLoading();
}

function joinGame() {
    hideAll();
    showJoinGame();
}

function joinGameConfirm() {

    showLoading();
    my_username = $("#join-game-username").val();

    setTimeout(function () {
        $.post(
            "api.php", {
                action: "create_user",
                name: my_username
            }, function (result) {
                hideAll();
                hideLoading();
                showClientGame();
            }
        );
    }, EFFECT_TIME);

}

function btnReady() {
    showLoading();
}

function addAction() {
    //showPopupAddAction();
}

function addActionItem(action) {

    var i = action_list.length;
    var action_icon;

    action_list.push(action);

    switch (action) {
        case "move":
            action_icon = "move";
            break;
        case "rotate":
            action_icon = "rotate-right";
            break;
        case "rotate-left":
            action_icon = "rotate-left";
            break;
        case "pick1":
            action_icon = "pick2";
            break;
        case "pick2":
            action_icon = "pick1";
            break;
        case "use1":
            action_icon = "use1";
            break;
        case "use2":
            action_icon = "use2";
            break;
    }

    var html = '<div data-index="' + last_item + '">' +
        '<div class="action-item" data-action="' + action +
        '"><img src="img/btn/' + action_icon + '.png' +
        '"><a href="#" data-index="' + last_item + '" onclick="removeActionItem(' + last_item++ + ')" >x</a></div>' +
        '<center><i class="fa fa-arrow-down"></i></center>' +
        '</div>';

    if (action == 'while') {
        html = '<div class="sort" data-index="' + last_item + '">' +
            '</div>';
    }
    /*if(action == "use1" || action == "use2"){
        html = '<div data-index="' + last_item + '">' +
            '<div class="action-item" data-action="' + action +
            '"><img src="img/btn/' + action_icon + '.png' +
            '"><a href="#" data-index="' + last_item + '" onclick="removeActionItem(' + last_item++ + ')" >x</a></div>' +
            '<center><i class="fa fa-arrow-down"></i></center>' +
            '</div>';
    }*/

    $("#action-list").append(html);

    $(".sort").sortable();

    $("#action-list").disableSelection();
    changed = true;

}

function removeActionItem(i) {
    $("div[data-index='" + i + "']").remove();
}


function startGameLoop() {
    game_loop = setInterval(gameLoop, GAME_LOOP_TIME);
}

function stopGameLoop() {
    clearInterval(game_loop);
}


function gameLoop() {

    action_list = [];

    $("#action-list .action-item").each(function (i) {
        action_list.push($(this).attr("data-action"));
    });

    $.post(
        "api.php", {
            action: "game_loop",
            action_list: action_list,
            changed: changed
        },
        gameLoopCallback
    );

}


function gameLoopCallback(data) {

    changed = false;

    data = JSON.parse(data);
    //console.log(data);

    if (data.status != last_game_status) {

        last_game_status = data.status;

        switch (last_game_status) {

            case "EP": // espera pela mae do tiago

                showLoading();

                break;

            case "P": // ja pode comer

                clearActionList();
                hideLoading();

                break;

            case "S": // pausa

                showLoading();

                break;

            case "F": // ja se veio

                alert(data.winner_id + " ganhou o jogo!");
                hideAll();
                showMainMenu();

                break;

        }

    }

}


function clearActionList() {
    $("#action-list").empty().append("<div></div>");
    $(".sort").sortable();

}


/* ******************************************* */


function hideAll() {
    $("#main-menu").fadeOut(EFFECT_TIME);
    $("#join-game").fadeOut(EFFECT_TIME);
    $("#client-game").fadeOut(EFFECT_TIME);
}

function showMainMenu() {
    $("#main-menu").fadeIn(EFFECT_TIME * 2);
}

function showJoinGame() {
    $("#join-game").fadeIn(EFFECT_TIME);
}

function showClientGame() {
    action_list = [];
    $("#label-username").text(my_username);
    $("#popup-add-action").hide();
    $("#client-game").fadeIn(EFFECT_TIME);

    $("#add-action-list a").draggable({
        connectToSortable: ".sort",
        containment: "document",
        revert: "invalid",
        helper: function () {
            changed = true;
            return '<div data-index="' + last_item + '">' +
                '<div class="action-item" data-action="' + $(this).attr("data-action") +
                '"><i class="fa fa-' + $(this).attr("data-icon") +
                '"></i><a href="#" data-index="' + last_item + '" onclick="removeActionItem(' + last_item++ + ')" >x</a></div>' +
                '<center><i class="fa fa-arrow-down"></i></center>' +
                '</div>';
        },
        start: function () {

        },
        stop: function () {
            $("#action-list").trigger("create");
        }
    });

    clearActionList();
    startGameLoop();
}

function showPopupAddAction() {
    $("#popup-add-action").fadeIn(EFFECT_TIME);
}

/* ******************************************* */


function showLoading() {
    $("#loading-screen").stop().fadeIn(EFFECT_TIME);
}

function hideLoading(delay = 0) {
    $("#loading-screen").delay(delay).fadeOut(EFFECT_TIME);
}


/* ******************************************* */
