<?php


session_start();


$servername = "localhost";
$username = "root";
$password = "";
$dbname = "shift";

$conn = mysqli_connect($servername, $username, $password, $dbname);


if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}
$action = $_REQUEST['action'];


switch ($action) {
    case 'create_user':
        $user = array("name" => $_REQUEST['name']);
        createUser($user, $conn);
        break;
    case 'create_game':
        $user = array("id" => $_REQUEST['id'], "operations" => $_REQUEST['operations']);
        createGame($user, $conn);
        break;
    case 'create_game_single':
        $user = array("id" => $_SESSION['user_id'], "operations" => $_REQUEST['operation']);
        createGame($user, $conn);
        break;
    case 'get_operations':
        getUsersOperations($conn);
        break;
    case 'game_over':
        $user = array("id" => $_SESSION['user_id']);
        gameOver(1, $user, $conn);
        break;
    case 'can_play':
        canPlay(1, $conn);
        break;
    case 'pause_game':
        stopSession(1, $conn);
        break;
    case 'resume_game':
        resumeSession(1, $conn);
        break;
    case 'game_loop':
        $action = [];
        if (isset($_REQUEST['action_list'])) {
            $action = $_REQUEST['action_list'];
        }
        getSession($conn, $action, $_REQUEST['changed']);
        break;
}

function stopSession($session_id, $conn)
{
    $sql = "Truncate games";
    $conn->query($sql);

    if ($conn->query($sql) === TRUE) {

    } else {
        die("Error");
    }

    $sql = "UPDATE sessions 
            SET status = 'S' WHERE 
            id = $session_id";
    if ($conn->query($sql) === TRUE) {

    } else {
        echo ' Error updating game!!';
    }
}

function resumeSession($session_id, $conn)
{
    $sql = "UPDATE sessions 
            SET status = 'P' WHERE 
            id = $session_id";
    if ($conn->query($sql) === TRUE) {
        $sql = "SELECT * from users
                  WHERE users.id in (1,2) ";

        $result = $conn->query($sql);

        $rows = array();
        while ($r = $result->fetch_assoc()) {
            $rows[] = $r;
        }


        echo json_encode($rows);
    } else {
        echo ' Error updating game!!';
    }


}


function canPlay($session_id, $conn)
{
    $sql = " SELECT status FROM sessions
          WHERE status = 'P' AND id = $session_id";

    $result = $conn->query($sql);
    if ($result->num_rows > 0)
        return true;

    return false;

}

function gameOver($session_id, $u, $conn)
{

    $sql = "UPDATE sessions 
            SET status = 'F' WHERE 
            id = $session_id AND winner_id ={$u['id']}";
    if ($conn->query($sql) === TRUE) {

    } else {
        echo ' Error finishing game!!';
    }
}


function getUsersOperations($conn)
{

    $sql = "SELECT users.id,users.name,games.operations FROM games INNER JOIN users
                  ON users.id= games.user_id 
                  WHERE users.id in (1,2) ";

    $result = $conn->query($sql);

    $rows = array();
    while ($r = $result->fetch_assoc()) {
        $rows[] = $r;
    }


    echo json_encode($rows);

    $sql = "Truncate games;";
    $conn->query($sql);

}

function createUser($u, $conn)
{

    $sql = "INSERT INTO users (name)
            VALUES ('{$u['name']}')";

    if ($conn->query($sql) === TRUE) {
        $last_id = $conn->insert_id;
        $_SESSION['user_id'] = $last_id;
        $sql = "SELECT * from users where id = $last_id ";
        $result = $conn->query($sql);
        while ($row = $result->fetch_assoc()) {
            echo json_encode($row);
        }
    } else {
        echo "Error: " . $sql . "<br>" . $conn->error;
    }

    $sql = "SELECT * FROM sessions where id = 1 ";
    $result = $conn->query($sql);
    if ($result->num_rows < 1) {
        $sql = "INSERT INTO sessions (status) VALUES ('EP') ";
        if ($conn->query($sql) === TRUE) {
            $_SESSION['session_id'] = 1;
        }
    }
    if (!isset($_SESSION['session_id'])) {
        $_SESSION['session_id'] = 1;
    } else {
        $_SESSION['session_id'] = 1;
    }

    $sql = "INSERT INTO games (id, session_id, user_id) VALUES ";
    $sql .= "( null, " . $_SESSION['session_id'] . "," . $_SESSION['user_id'] . ")";

    if ($conn->query($sql) === TRUE) {
        echo "New record created successfully";
    } else {
        echo "Error: " . $sql . "<br>" . $conn->error;
    }


    $conn->close();
}


/**
 * @param $conn
 * @param array $operations
 */
function getSession($conn, $operations = array(), $changed)
{

    if ($changed == "true") {

        $sql = "DELETE from games where session_id = {$_SESSION['session_id']}  AND user_id={$_SESSION['user_id']}";
        $conn->query($sql);

        if ($conn->query($sql) === TRUE) {

        } else {
            die("Error");
        }


        if (count($operations)) {
            $sql = " INSERT INTO games (id, session_id, user_id,operations) VALUES ";

            foreach ($operations as $k => $v) {
                if ($k == 0)
                    $sql .= "( null, " . $_SESSION['session_id'] . "," . $_SESSION['user_id'] . ", '" . $v . "')";
                else
                    $sql .= ",( null, " . $_SESSION['session_id'] . "," . $_SESSION['user_id'] . ", '" . $v . "')";
            }


            if ($conn->query($sql) === TRUE) {
            } else {
                echo "Error: " . $sql . "<br>" . $conn->error;
            }
        }


    }


    $sql = "SELECT sessions.*,users.name as winner  from 
                sessions
                LEFT  JOIN users  ON sessions.winner_id = users.id
                WHERE sessions.id = {$_SESSION['session_id']}";


    $result = $conn->query($sql);

    $rows = array();
    while ($r = $result->fetch_assoc()) {
        $rows[] = $r;
    }

    echo json_encode($rows[0]);


}


function createGame($u, $conn)
{

    //$sql = "Truncate games;";
    //$conn->query($sql);
    var_dump($_SESSION);
    die;
    if (!isset($_SESSION['session_id'])) {
        $sql = "INSERT INTO sessions (status) VALUES ('EP') ";
        if ($conn->query($sql) === TRUE) {
            $last_id = $conn->insert_id;
            $_SESSION['session_id'] = $last_id;
        }
    }

    $sql = "INSERT INTO games (id, session_id, user_id) VALUES ";
    $sql .= "( null, " . $_SESSION['session_id'] . "," . $u['id'] . "')";

    if ($conn->query($sql) === TRUE) {
        echo "New record created successfully";
    } else {
        echo "Error: " . $sql . "<br>" . $conn->error;
    }


    $conn->close();
}

?>